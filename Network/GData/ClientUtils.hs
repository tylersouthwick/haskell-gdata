module Network.GData.ClientUtils(loadToken) where

import qualified Network.GData.Authorization as GAuth
import qualified Data.ByteString.Char8 as BS

requestToken auth f = do
  putStrLn "Go to the following URL in your browser: "
  putStrLn (BS.unpack (GAuth.authorize auth))
  putStrLn "Paste the code here: "
  code <- getLine
  token <- GAuth.requestAccessToken auth (BS.pack code)
  writeFile f (show token)
  putStrLn "Got access token"
  return token

convertToken s = (read s) :: Maybe GAuth.AccessToken

doLoadToken auth f = do
  s <- readFile f 
  if s == "" then do
    token <- requestToken auth f
    return token
  else do
    putStrLn "Loaded access token"
    let token = convertToken s
    return token

loadToken auth f success failure = do
  token <- doLoadToken auth f
  case token of
    Just actualToken -> do
      success actualToken
    Nothing -> do
      failure
