module Network.GData.Authorization(AccessToken, accessToken, Scope(..), Authorization(..), authorize, requestAccessToken) where

import qualified Network.OAuth2.OAuth2 as OAuth2
import qualified Network.OAuth2.HTTP.HttpClient as OAuth2Client
import qualified Data.ByteString.Char8 as BS
import Network.HTTP.Types (renderSimpleQuery)

data Authorization =
    Authorization {
        clientId :: BS.ByteString,
        clientSecret :: BS.ByteString,
        redirectUrl :: BS.ByteString,
        authorizationScope :: Scope
    } deriving (Eq, Show, Read)

data Scope = UserInfo | Email | Profile | Contacts | Tasks | TasksReadOnly | Drive deriving (Show, Read, Eq, Enum)

convertScope UserInfo = "https://www.googleapis.com/oauth2/v1/userinfo"
convertScope Email = "https://www.googleapis.com/auth/userinfo.email"
convertScope Profile = "https://www.googleapis.com/auth/userinfo.profile"
convertScope Contacts = "https://www.google.com/m8/feeds"
convertScope Tasks = "https://www.googleapis.com/auth/tasks"
convertScope TasksReadOnly = "https://www.googleapis.com/auth/tasks.readonly"
convertScope Drive = "https://www.googleapis.com/auth/drive.file"

data AccessToken = AccessToken {
  accessToken :: BS.ByteString
} deriving (Show, Read)

googleOAuthEndpoint = BS.pack "https://accounts.google.com/o/oauth2/auth"

createOAuth :: Authorization -> OAuth2.OAuth2
createOAuth auth = OAuth2.OAuth2 {
  OAuth2.oauthClientId = clientId auth,
  OAuth2.oauthClientSecret = clientSecret auth,
  OAuth2.oauthOAuthorizeEndpoint = googleOAuthEndpoint,
  OAuth2.oauthAccessTokenEndpoint = BS.pack "https://accounts.google.com/o/oauth2/token",
  OAuth2.oauthCallback = Just (redirectUrl auth),
  OAuth2.oauthAccessToken = Nothing
}

authorizationUrl auth = OAuth2.authorizationUrl (createOAuth auth)
googleScope scope = renderSimpleQuery False [(BS.pack "scope", BS.pack (convertScope scope))]
append url scope = BS.append (BS.append url (BS.pack "&")) scope
authorize auth = append (authorizationUrl auth) (googleScope (authorizationScope auth))

convertToken token = maybe Nothing (\y -> Just (AccessToken (OAuth2.accessToken y))) token

requestAccessToken auth code = do
  token <- OAuth2Client.requestAccessToken (createOAuth auth) code
  return (convertToken token)
