{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveDataTypeable #-}
{-# LANGUAGE FlexibleContexts #-}

module Network.GData.Tasks(listTasks) where

import qualified Network.GData.Authorization as Auth
import Network.HTTP.Conduit
import Control.Monad.Trans (liftIO)

taskListsUrl = "https://www.googleapis.com/tasks/v1/users/@me/lists"

listTasks token = liftIO $ withManager $ \man -> do
  initReq <- parseUrl taskListsUrl
  let req = initReq {
    requestHeaders = [("Authorization", Auth.accessToken token)]
  }
  httpLbs req man
