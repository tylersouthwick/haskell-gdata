module Network.GData.Contacts(findContact) where

data Contact =
    Contact {test :: String}
    deriving (Eq, Show, Read)

findContact = Contact("hello")
